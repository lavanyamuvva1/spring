package com.epam.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.epam.dto.request.TraineeDto;
import com.epam.dto.request.TraineeTrainingsList;
import com.epam.dto.request.TraineeUpdateDto;
import com.epam.dto.response.CredentialsDto;
import com.epam.dto.response.NotificationDto;
import com.epam.dto.response.TraineeProfileDto;
import com.epam.dto.response.TrainerDetailsDto;
import com.epam.dto.response.TrainingDetailsDto;
import com.epam.entity.Trainee;
import com.epam.entity.Trainer;
import com.epam.entity.Training;
import com.epam.entity.User;
import com.epam.exception.UserException;
import com.epam.kafka.Producer;
import com.epam.repository.TraineeRepository;
import com.epam.repository.TrainerRepository;
import com.epam.repository.TrainingRepository;
import com.epam.repository.UserRepository;
import com.epam.utils.ServiceMapper;

import jakarta.transaction.Transactional;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class TraineeServiceImpl implements TraineeService {

	@Autowired
	TraineeRepository traineeRepository;

	@Autowired
	TrainingRepository trainingRepository;

	@Autowired
	UserRepository userRepository;

	@Autowired
	TrainerRepository trainerRepository;

	@Autowired
	ServiceMapper serviceMapper;

	@Autowired
	Producer producer;

	static final String TRAINEE_EXCEPTION = "Trainee with username not found";

	@Override
	public CredentialsDto addTrainee(TraineeDto traineeDto) {
		log.info("Entered into Create Trainee Method :{}", traineeDto);
		User user = serviceMapper.createUserTraineeProfile(traineeDto);
		userRepository.save(user);
		Trainee trainee = Trainee.builder().address(traineeDto.getAddress()).dateOfBirth(traineeDto.getDateOfBirth())
				.user(user).build();
		traineeRepository.save(trainee);
		CredentialsDto credentialsDto = CredentialsDto.builder().username(user.getUsername())
				.password(user.getPassword()).build();
		NotificationDto dto = serviceMapper.getRegistrationNotification(user);
		producer.sendNotificationLog(dto);
		return credentialsDto;

	}

	@Override
	public TraineeProfileDto getTraineeProfile(String username) {
		log.info("Entered into get Trainee Profile of {}", username);
		return traineeRepository.findByUserUsername(username).map(trainee -> {
			return serviceMapper.getTraineeProfileDto(trainee);
		}).orElseThrow(() -> new UserException(TRAINEE_EXCEPTION));

	}

	@Override
	@Transactional
	public TraineeProfileDto updateTraineeProfile(TraineeUpdateDto traineeUpdateDto) {
		log.info("Entered into update Trainee Profile of {}", traineeUpdateDto);
		return traineeRepository.findByUserUsername(traineeUpdateDto.getUsername()).map(trainee -> {
			User user = trainee.getUser();
			user.setEmail(traineeUpdateDto.getEmail());
			user.setFirstName(traineeUpdateDto.getFirstName());
			user.setLastName(traineeUpdateDto.getLastName());
			user.setActive(traineeUpdateDto.isActive());
			trainee.setAddress(traineeUpdateDto.getAddress());
			trainee.setDateOfBirth(traineeUpdateDto.getDateOfBirth());
			NotificationDto dto = serviceMapper.getTraineeUpdateNotification(traineeUpdateDto);
			producer.sendNotificationLog(dto);
			log.info("Retriving Updated trainee Profile");
			return serviceMapper.getTraineeProfileDto(trainee);
		}).orElseThrow(() -> new UserException(TRAINEE_EXCEPTION));

	}

	@Override
	@Transactional
	public void deleteTrainee(String userName) {
		log.info("Entered into delete trainee profile of {}", userName);
		Trainee trainee = traineeRepository.findByUserUsername(userName)
				.orElseThrow(() -> new UserException(TRAINEE_EXCEPTION));
		for (Trainer t : trainee.getTrainerList()) {
			t.getTraineeList().remove(trainee);
		}
		log.info("Deleting the trainee profile");
		traineeRepository.deleteById(trainee.getId());
	}

	@Override
	@Transactional
	public List<TrainerDetailsDto> updateTraineeTrainersList(String username, List<String> trainerUsernames) {
		log.info("Entered into update Trainee TrainersList of username {}", username);
		Trainee trainee = traineeRepository.findByUserUsername(username)
				.orElseThrow(() -> new UserException(TRAINEE_EXCEPTION));

		List<Trainer> trainersToAdd = trainerUsernames.stream()
		        .map(s -> trainerRepository.findByUserUsername(s))
		        .filter(Optional::isPresent)
		        .map(Optional::get)
		        .filter(trainer -> !trainee.getTrainerList().contains(trainer))
		        .toList();


		List<Trainer> trainersToRemove =trainee.getTrainerList().stream()
        .filter(t -> !trainerUsernames.contains(t.getUser().getUsername()))
        .collect(Collectors.toList());

		trainee.getTrainerList().addAll(trainersToAdd);
		trainee.getTrainerList().removeAll(trainersToRemove);
		log.info("Retriving trainer profile details after updating");
		return serviceMapper.getTrainerDetailsList(trainee.getTrainerList());

	}

	@Override
	public List<TrainerDetailsDto> getNotAssignedActiveTrainers(String username) {
		log.info("Entered into getNotAssignedActiveTrainers of {}", username);
		return traineeRepository.findByUserUsername(username).map(trainee->{
			List<Trainer> notAssignedTrainers = trainerRepository.findByTraineeListNotContaining(trainee);
			return serviceMapper.getTrainerDetailsList(notAssignedTrainers);
		}).orElseThrow(() -> new UserException(TRAINEE_EXCEPTION));
		
	}

	@Override
	public List<TrainingDetailsDto> getTraineeTrainingsList(TraineeTrainingsList traineeTrainingsList) {
		log.info("Entered into getTraineeTrainingsList");
		return traineeRepository.findByUserUsername(traineeTrainingsList.getUsername()).map(trainee -> {
			List<Training> trainingsList = traineeRepository.findTrainingsForTrainee(traineeTrainingsList.getUsername(),
					traineeTrainingsList.getPeriodFrom(), traineeTrainingsList.getPeriodTo(),
					traineeTrainingsList.getTrainerName(), traineeTrainingsList.getTrainingType());
			log.info("Retriving training details of trainee");
			return serviceMapper.getTrainingDetailsList(trainingsList);
		}).orElseThrow(() -> new UserException(TRAINEE_EXCEPTION));

	}

}
