package com.epam.kafka;

import org.apache.kafka.clients.admin.NewTopic;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.config.TopicBuilder;

@Configuration
public class Topic {
	
	@Value("${topic1-name}")
	private String notificationTopic;
	
	@Value("${topic2-name}")
	private String reportTopic;
	
	public NewTopic getReportTopic()
	{
		return TopicBuilder.name("reportTopic").build();
	}
	
	public NewTopic getNotificationTopic()
	{
		return TopicBuilder.name(notificationTopic).build();
	}

}
