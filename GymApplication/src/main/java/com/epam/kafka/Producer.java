package com.epam.kafka;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Service;
import org.springframework.messaging.Message;

import com.epam.dto.request.TrainingReportDto;
import com.epam.dto.response.NotificationDto;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class Producer {
	
	private final KafkaTemplate<String,TrainingReportDto> kafkaReportTemplate;
	private final KafkaTemplate<String,NotificationDto> kafkaNotificationTemplate;
	
	public void sendGymReport(TrainingReportDto dto)
	{
		kafkaReportTemplate.send("report-topic",dto);
	}
	public void sendNotificationLog(NotificationDto dto)
	{
		kafkaNotificationTemplate.send("notification-topic",dto);
	}

}

